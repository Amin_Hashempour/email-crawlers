from bs4 import BeautifulSoup
import requests

email_list = []
link_list = []
for i in range(1,11):

	url = 'https://www.e-estekhdam.com/search/%D8%A7%D8%B3%D8%AA%D8%AE%D8%AF%D8%A7%D9%85-%D8%AF%D8%B1-%D8%AA%D9%87%D8%B1%D8%A7%D9%86?page=' + str(i)
	source = requests.get(url)
	soup = BeautifulSoup(source.text , 'lxml')


	link_container = soup.find_all("div" , class_="job-list-item")
	for item in link_container:
		link = 'https://www.e-estekhdam.com/' + str(item.find("a"))
		link_list.append(link)

	for item in link_list:
		url = item
		source = requests.get(url)
		soup = BeautifulSoup(source.text, 'lxml')
		container = soup.find("div", class_="contact-email")
		if container != None :
			container2 = container.find("span" , class_="contact-data")
			container3 = container2.find("a")['href']
			if container3 is not None :
				email = container3.replace('mailto:' , '')
				email_list.append(email)
				print(email)
