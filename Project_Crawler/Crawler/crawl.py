import time
import queue
import threading

from bs4 import BeautifulSoup, SoupStrainer
import urllib.parse
import urllib.request

import config
import models

AGENTS = [
    {'is_running': False, 'agent_number':i, 'status': 'on'} for i in range(3)
]
URL_QUEUE = queue.Queue()
SEEN_URLS = list()


def start_agent(agent, max_depth):

    url = URL_QUEUE.get()
    print('agent {} started for {}'.format(agent['agent_number'],url[0]),'\n\n')

    page_url = url[0]
    parent_id = url[1]
    parent_url = url[2]
    page_depth = url[3]
    page_tags = url[4]

    if page_depth > max_depth:
        idx = AGENTS.index(agent)
        AGENTS[idx]['status'] = 'off'
        agent['is_running'] = False
        print('agent {} turned off'.format(agent['agent_number']))
        return

    
    if page_url in SEEN_URLS:
        print('agent {} : url ({}) is already seen!'.format(agent['agent_number'], page_url))
        agent['is_running'] = False
        return

    session = models.get_db_session(config.DB_ENGINE_URI)
    page_source = get_page_source(page_url)
    page_title = get_page_title(page_source).encode('utf-8')
    page_tags = get_page_tags(page_url,page_tags)

    page = models.Page(
        title = page_title,
        url = page_url.encode('utf-8'),
        parent_id = parent_id,
        parent_url = parent_url.encode('utf-8'),
        depth = page_depth
    )

    session.add(page)
    session.commit()

    for tag in page_tags:
        if str(tag).startswith('<a') :
            tag_content = tag.get('href')
        elif str(tag).startswith('<img') :
            tag_content = tag.get('src')
        else :
            tag_content = tag.text
        root_page_tags = models.Tag(
            tag = tag,
            content = tag_content,
            page_id = parent_id,
            page_url = page_url.encode('utf-8')          
        )
        session.add(root_page_tags)

    session.commit()

    SEEN_URLS.append(page_url)

    new_urls = page_urls(page_url, page_source)

    _id = int(page.id)

    for url in new_urls[:20]:
        if url not in SEEN_URLS :
            URL_QUEUE.put([url, _id, page_url, page_depth + 1, page_tags])
   
    agent['is_running'] = False
    print('agent {} end'.format(agent['agent_number']))


def get_page_source(url):
    # print('getting page source: ' + url)
    try:
        response = urllib.request.urlopen(url)
    except UnicodeEncodeError:
        print('bad url: ' + url)
        response = urllib.request.urlopen(url.encode('utf-8'))
        # exit()
    except:
        return ''
    try:
        page_source = response.read()
        return page_source
    except:
        return ''


def get_page_tags(url,tags):
    tags_list = tags
    response = urllib.request.urlopen(url)
    page_source = response.read()
    soup = BeautifulSoup(page_source, 'html.parser')
    soup_tag = soup.find_all(tags_list)
    return soup_tag
    

def page_urls(page_url, page_source):
    new_urls = []
    p = urllib.parse.urlparse(page_url)
    bs = BeautifulSoup(page_source, 'html.parser',parse_only=SoupStrainer('a'))
    for a_tag in bs:
        if a_tag.has_attr('href'):
            href = a_tag['href']
            if href != '' and href[0] != '#':
                if href[:2] == '//':
                    new_url = p.scheme + ':' + href
                elif href[0] == '/':
                    new_url = p.scheme + '://' + p.hostname + href
                else:
                    new_url = href

                if new_url.startswith(config.ROOT_URL):
                    new_urls.append(new_url)

    return new_urls


def get_page_title(page_source):
    soup = BeautifulSoup(page_source, 'html.parser')
    try:
        title = soup.title.string
    except:
        title = ''
    return str(title)


def main():

    input_url = input('url: ')
    input_max_depth = input('max depth: ')
    input_tags = input('tags: ')
    
    if input_url :
        root_url = str(input_url)
    else :
        root_url = config.ROOT_URL

    if input_tags :
        input_tags = input_tags.split(',')
    else :
        input_tags = None

    if input_max_depth :
        input_max_depth = int(input_max_depth)
    else :
        input_max_depth = float('inf')

    session = models.get_db_session(config.DB_ENGINE_URI)

    root_page_source = get_page_source(root_url)
    root_page_title = get_page_title(root_page_source).encode('utf-8')
    page_tags = get_page_tags(root_url,input_tags)

    # commit pages
    root_page = models.Page(
        title = root_page_title,
        url = root_url.encode('utf-8'),
        parent_id = -1,
        parent_url = '',
        depth = 0
    )
    session.add(root_page)
    session.commit()
    
    # commit tags
    for tag in page_tags:
        if str(tag).startswith('<a') :
            tag_content = tag.get('href')
        elif str(tag).startswith('<img') :
            tag_content = tag.get('src')
        else :
            tag_content = tag.text
        root_page_tags = models.Tag(
            tag = tag,
            content = tag_content,
            page_id = -1,
            page_url = root_url.encode('utf-8')          
        )
        session.add(root_page_tags)

    session.commit()

    SEEN_URLS.append(root_url)
    
    new_urls = page_urls(root_url, root_page_source)

    root_id = int(root_page.id)
    depth = 1

    for url in new_urls[:10]:
        if url not in SEEN_URLS:
            URL_QUEUE.put([url, root_id, root_url, depth, input_tags])
            # print('url added')

    print('length of urls : {}'.format(len(new_urls)),'\n')

    while not URL_QUEUE.empty():
        # print(URL_QUEUE)
        for agent in AGENTS:
            if not agent['is_running'] and agent['status'] == 'on' and not URL_QUEUE.empty():
                # time.sleep(1.7)
                thread = threading.Thread(target=start_agent, args=(agent, input_max_depth))
                agent['is_running'] = True
                thread.start()

        all_agents_off = True
        for agent in AGENTS:
            if agent['status'] == 'on':
                all_agents_off = False
                break

        if all_agents_off:
            print('all agents are off so exiting')
            exit()


main()

print('URL_QUEUE is empty so exiting')


