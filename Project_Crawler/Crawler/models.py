from sqlalchemy import (
    Column, Integer, String, Boolean, DateTime, Text, create_engine
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import func
from sqlalchemy.pool import SingletonThreadPool
from sqlalchemy_utils import create_database, database_exists


Base = declarative_base()


def get_db_session(db_engine_url, drop=False):
    if not database_exists(db_engine_url):
        create_database(db_engine_url)

    engine = create_engine(db_engine_url, encoding='utf-8', poolclass=SingletonThreadPool, pool_recycle=18000)

    if drop:
        Base.metadata.drop_all(engine)

    Base.metadata.create_all(engine)

    Session = sessionmaker(bind=engine)
    session = Session()

    return session


class Page(Base):
    __tablename__ = 'Pages'
    id = Column(Integer, autoincrement=True, primary_key=True, index=True)
    title = Column(String(100, convert_unicode=True))
    url = Column(String(500, convert_unicode=True), index=True)
    parent_id = Column(Integer, index=True)
    parent_url = Column(String(500, convert_unicode=True), index=True)
    depth = Column(Integer, index=True)


class Tag(Base):
    __tablename__ = 'Tags'
    id = Column(Integer, autoincrement=True, primary_key=True, index=True)
    tag = Column(String(50), index=True)
    content = Column(Text)
    page_id = Column(String(50), index=True)
    page_url = Column(String(500, convert_unicode=True), index=True)