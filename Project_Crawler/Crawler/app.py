from flask import Flask, render_template

import models
import config

app = Flask(__name__)

session = models.get_db_session(config.DB_ENGINE_URI)

@app.route("/pages")
def pages():
    pages_objects = session.query(models.Page).all()
    return render_template('pages.html', pages_objects=pages_objects)

@app.route("/tags")
def tags():
    tags_objects = session.query(models.Tag).all()
    return render_template('tags.html', tags_objects=tags_objects)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8282, debug=True)