from telethon import TelegramClient, events, sync
import sys
import time
import asyncio
import aiohttp
import auth
import logging
import re

api_id = 1086697
api_hash = '726289d1f65a69aba48bd158d7086dd7'
client = TelegramClient('session_name', api_id, api_hash)
client.start()


async def main():
	post_list = []
	link_list = []

	async for message in client.iter_messages('MBAestekhdam'):
		post_list.append(message.text)

	for item in post_list:
		tmp = re.compile("[s]?(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+")
		url = tmp.findall(str(item))
		for item in url:
			if '@' in item:
				if len(item) >= 3:
					link_list.append(item)
	link_list = list(dict.fromkeys(link_list))

	for item in link_list:
		tmp = re.compile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
		url = tmp.findall(str(item))
		for item in url:
			print(item)

with client:
	client.loop.run_until_complete(main())
