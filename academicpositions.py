from bs4 import BeautifulSoup
import requests

for i in range (1,30):
	url = 'https://academicpositions.com/jobs/electrical-engineering-engineering?page=' + str(i)
	source = requests.get(url)
	soup = BeautifulSoup(source.text, 'lxml')


	container1 = soup.find("div", class_= "section js-gtm-jobslist")
	container2 = container1.find_all("div" , class_= "job__image")
	container3 = container1.find_all("a", class_= "job__title js-gtm-joblink")

	img_list = []
	title_list = []
	link_list = []
	if len(container2) != 0:
		for item in container2:
			img_list.append(("https://academicpositions.com" + item.find("img")['src']))
		print(img_list)
	else:
		break


	if len(container3) != 0:
		for item in container3:
			title_list.append((item.text))
			link_list.append(('https://academicpositions.com' + item.get('href')))
		print(title_list)
		print(link_list)
	else:
		break
